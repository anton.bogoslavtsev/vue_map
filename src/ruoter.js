import Map from './components/Map.vue'
import VueRouter from 'vue-router'

export default new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Map }
    ]
})